package FetchCVE;

use strict;
use warnings;
use Carp;
use LWP;
use HTML::TokeParser;
use Data::Dumper;


#------------------------------------------------------------
require Exporter;
our @ISA = qw(Exporter);
our %EXPORT_TAGS = ( 'all' => [ qw( fetchcve ) ] );
our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
our @EXPORT = qw( fetchcve );
use version; our $VERSION = qv('0.1.2');

sub fetchcve {
    my $cve = shift;
    my $timeout = 10;
    my $baseURL      = 'http://web.nvd.nist.gov/view/vuln/detail?vulnId=';
    my $host         = 'web.nvd.nist.gov';
    my $ua;
    my $url = $baseURL . $cve;
    my ($html, $response);
    my ($staticLink, $score, $overview, @vulnerable, @references );
    my $metric;
    my $suggest_dos = 0;
    my $date;

    $ua = LWP::UserAgent->new;
    $ua->agent(
        "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.4; en-GB; rv:1.9.0.11) Gecko/2009060214 Firefox/3.0.11"
    );

    $ua->timeout($timeout);
    $ua->requests_redirectable;
    $ua->default_headers->push_header( 'Keep-Alive' => '300' );
    $ua->default_headers->push_header( 'Proxy-Connection' => 'keep-alive' );
    $response = $ua->get($url);

    my $code = $response->code;
    my $base = $response->base;

    if ( $code == 302 ) {
        # Make another request with the referrer
        my $request = HTTP::Request->new( GET => $base );

        # Set the referrer
        $request->referer($url);
        $response = $ua->request($request);

        croak "Can't get $base -- ", $response->status_line
            unless $response->is_success;
        croak "No content found" unless $response->content;
        $html = $response->content;

    } elsif ( $code == 200 ) {
        $html = $response->content;
    } else {
        croak "Error, HTTP Response Code $code\n Status: " . $response->status_line;
        exit();
    }

    my $index;
    my $found;
    my @next;
    my $stream = HTML::TokeParser->new( \$html );


    while ( my $token = $stream->get_token ) {

        #----------------------------------------------------------------------
        # (1) Static Link: Look for the following sequence:
        #        <div class="row"><span class="label">Static
        #        Link:</span> <a
        #        href="http://web.nvd.nist.gov/view/vuln/detail;
        # jsessionid=a8d963b180c2ff0b7e01ff2a997d?vulnId=CVE-2009-0884">
        # http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2009-0884</a>

        if ( $token->[0] eq 'T' and $token->[1] eq 'Static Link:' ) {

            @next = ();
            push @next, $stream->get_token();
            $found = 0;
            $index = 0;

            # skip the End span tag
            if ( $next[$index][0] eq 'E' and $next[$index][1] eq 'span' ) {
                push @next, $stream->get_token();
                $index++;

                #if this is blank text then skip it
                if ( $next[$index][0] eq 'T' ) {
                    push @next, $stream->get_token();
                    $index++;
                }

                if ( $next[$index][0] eq 'S' and $next[$index][1] eq 'a' ) {
                    push @next, $stream->get_token();
                    $index++;

                    if ( $next[$index][0] eq 'T' ) {
                        $staticLink = $next[$index][1];
                        $found = 1;
                    }
                }
            }
        }

        #----------------------------------------------------------------------
        # (1b) Original Release Date:
        #<div class="vulnDetail">
        #   <h2>National Cyber-Alert System</h2>
        #  <h3>Vulnerability Summary for CVE-2011-3170
        #  </h3>
        #  <div class="row"><span class="label">Original release date:</span>08/19/2011</div>
        #  <div class="row"><span class="label">Last revised:</span>09/23/2011
        #      </div>

        if (    $token->[0] eq 'T'
                    and $token->[1] eq 'National Cyber-Alert System' ) {
            @next = ();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();
            push @next, $stream->get_token();

            $date = $next[10][1];
            $found = 1;
            $index = 0;
        }


        #----------------------------------------------------------------------
        # (2) Overview Text: Look for the following sequence:
        #<h4>Overview</h4>
        #<p>Example text</p>

        if (    $token->[0] eq 'T'
                    and $token->[1] eq 'Overview' ) {

            @next = ();
            push @next, $stream->get_token();


            $found = 0;
            $index = 0;

            if ( $next[$index][0] eq 'E' and $next[$index][1] eq 'h4' ) {

                push @next, $stream->get_token();
                $index++;


                #if this is blank text then skip it
                if ( $next[$index][0] eq 'T' ) {
                    push @next, $stream->get_token();
                    $index++;
                }

                if ( $next[$index][0] eq 'S' and $next[$index][1] eq 'p' ) {

                    push @next, $stream->get_token();
                    #print "\n****\n" . Dumper(@next) . "\n****\n\n";

                    $index++;

                    if ( $next[$index][0] eq 'T' ) {
                        $overview = $next[$index][1];
                        $found = 1;
                    }
                }
            }
            $stream->unget_token(@next) unless $found;
        }

        #----------------------------------------------------------------------
        # (3) CVSSv2 Score
        #
        # like this:
        # <h5>CVSS Severity (version 2.0):</h5>
        #		  <div class="row"><span class="label">CVSS v2 Base Score:
        # </span><a href="http://nvd.nist.gov/cvss.cfm?version=2&name=
        # CVE-2009-0884&vector=(AV%3AN/AC%3AL/Au%3AN/C%3AN/I%3AN/A%3AP)"
        # target="_blank">5.0</a> (MEDIUM) <a href="http://nvd.nist.gov/cvss.cfm?
        # version=2&name=CVE-2009-0884&vector=(AV%3AN/AC%3AL/Au%3AN/C%3AN/I%3AN/A%3AP)"
        # target="_blank">(AV:N/AC:L/Au:N/C:N/I:N/A:P)</a> (<a href=
        # "http://nvd.nist.gov/cvss.cfm?vectorinfo&version=2"
        # target="_blank">legend</a>)

        if ( $token->[0] eq 'T'
                    and $token->[1] eq 'CVSS v2 Base Score:' ) {
            @next = ();
            push @next, $stream->get_token();


            $found = 0;
            $index = 0;
            my $tempscore;

            # * Sun 28-Dec-2014 16:42:42  (David Walker) : added as workaround for
            # new extra line feed that suddenly appeared here.

            my $tmp = $stream->get_token();

            # unless ( $tmp[0] eq 'T' and $tmp[2] eq '' ) {
            #     $stream->unget_token($tmp);
            #     print Dumper $tmp;
            # }


            push @next, $stream->get_token();
            # push @next, $stream->get_token();
            # push @next, $stream->get_token();
            # push @next, $stream->get_token();
            # push @next, $stream->get_token();
            # push @next, $stream->get_token();
            # push @next, $stream->get_token();
            # push @next, $stream->get_token();

            #print "\n****\n" . Dumper(@next) . "\n****\n\n";


            # slip the End span tag
            if ( $next[$index][0] eq 'E' and $next[$index][1] eq 'span' ) {
                push @next, $stream->get_token();
                $index++;

                if ( $next[$index][0] eq 'S' and $next[$index][1] eq 'a' ) {
                    push @next, $stream->get_token();

                    $index++;

                    if ( $next[$index][0] eq 'T' ) {
                        $tempscore = $next[$index][1];
                        push @next, $stream->get_token();
                        $index++;

                        if ( $next[$index][0] eq 'E' and $next[$index][1] eq 'a' ) {
                            push @next, $stream->get_token();
                            $index++;

                            if ( $next[$index][0] eq 'T' ) {
                                push @next, $stream->get_token();
                                $index++;

                                if ( $next[$index][0] eq 'S' and $next[$index][1] eq 'a' ) {
                                    push @next, $stream->get_token();
                                    $index++;

                                    if ( $next[$index][0] eq 'T' ) {

                                        if ( $next[$index][1] =~ m/\((.+)\)/ ) {
                                            $metric = $1;
                                            $score = $tempscore;
                                            $found = 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $stream->unget_token(@next) unless $found;
        }
    }

    #----------------------------------------------------------------------
    # Extract Vulnerable Versions
    # eg <td><span class="fact">* cpe:/a:filezilla:filezilla_server:0.9.29</span></td>
    while ( $html =~ m{<td><span class="fact">(.*)</span></td>}g ) {
        push( @vulnerable, $1 );
    }

    #----------------------------------------------------------------------
    # Extract Other References:
    while ( $html
                =~ m{External Source</span>: (.*)\n.*\n.*Name:</span> (.*)</div>.*\n.*[\n]*.*_blank">(.*)</a>}g
            ) {

        my $ref = "* $1: $2\n$3\n";
        push( @references, $ref );
    }

    #----------------------------------------------------------------------
    # Other Results Possible are:
    # print "References:\n";
    # foreach (@references) {
    # print "$_\n";
    # }

    # print "Vulnerable Versions:\n";
    # foreach (@vulnerable) {
    # print "$_\n";
    # }

    if ( (defined $overview) && ($overview =~ m/denial/i) ) {

        if ( ($metric =~ m/\/C:N\/I:N\/A:P$/) || ($metric =~ m/\/C:N\/I:N\/A:C$/) ||
        ($metric =~ m/\/C:P\/I:P\/A:P$/) || ($metric =~ m/\/C:N\/I:P\/A:P$/) ) {

            if ($overview =~ m/execute\sarbitrary/i) {
                $suggest_dos = 0;
            }
            elsif ($overview =~ m/gain\sprivilege/i) {
                $suggest_dos = 0;
            }
            elsif ($overview =~ m/obtain\ssensitive\sinformation/i) {
                $suggest_dos = 0;
            }
            else{
                $suggest_dos = 1;
            }
        }
    }

    return ($score, $overview, $url, $metric, $suggest_dos, $date);
}



# Preloaded methods go here.

1;
__END__


=head1 NAME

FetchCVE - Perl Module to scrape information about a CVE from nvd.nist.gov


=head1 SYNOPSIS

  use FetchCVE;

  ($cvss, $desc, $url) = getCVE($cve);

=head1 DESCRIPTION

Uses LWP to fetch a web page from nvd.nist.gov for a single CVE.  This
HTML page is then parsed to extract the information we want.  This is
the CVSSv2 score and the short description 'Overview'.  The URL used
to fetch the data is also returned for futher reference.

=head2 EXPORT

None by default.

=head1 SEE ALSO

Original data can be found here: http://web.nvd.nist.gov/view/vuln/search?cid=4

=head1 AUTHOR

David Walker, E<lt>dwalker@handshake.hkE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2010-2013 by David Walker, Handshake Networking Ltd

=cut
